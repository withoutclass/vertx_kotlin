import com.beust.kobalt.buildScript
import com.beust.kobalt.plugin.application.application
import com.beust.kobalt.plugin.packaging.assemble
import com.beust.kobalt.project

val p = project {
    name = "vertx_api"
    group = "com.example"
    artifactId = name
    version = "0.1"
    val kotlinVersion = "1.1.2-5"

    dependencies {
        //        compile("com.beust:jcommander:1.68")
        compile("io.vertx:vertx-core:3.4.1")
        compile("io.vertx:vertx-web:3.4.1")
        compile("org.jetbrains.kotlinx:vertx3-lang-kotlin:0.0.15")
        compile("com.englishtown.vertx:vertx-cassandra:3.2.0")
        compile("org.jetbrains.kotlin:kotlin-reflect:${kotlinVersion}")
    }

    dependenciesTest {
        compile("org.testng:testng:6.11")
        compile("junit:junit:4.12")
        compile("io.vertx:vertx-unit:3.4.1")
    }

    assemble {
        jar {
            fatJar = true
            manifest {
                attributes("Main-Class", "io.vertx.core.Launcher")
                attributes("Main-Verticle", "com.zablorp.MyFirstVerticle")
            }
        }
    }

    application {
        mainClass = "com.zablorp.MyFirstVehicle"
    }

    val bs = buildScript {
        repos("http://dl.bintray.com/cy6ergn0m/maven")
    }
}