/**
 * Created by mattlangreder on 6/22/17.
 */

import com.zablorp.MyFirstVerticle
import io.vertx.core.Vertx
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(VertxUnitRunner::class)
class MyFirstVerticleTest {

    private var vertx: Vertx? = null

    @Before
    fun setUp(context: TestContext) {
        vertx = Vertx.vertx()
        vertx!!.deployVerticle(MyFirstVerticle::class.java.name,
                context.asyncAssertSuccess<String>())
    }

    @After
    fun tearDown(context: TestContext) {
        vertx!!.close(context.asyncAssertSuccess<Void>())
    }

    @Test
    fun testMyApplication(context: TestContext) {
        val async = context.async()

        vertx!!.createHttpClient().getNow(8080, "localhost", "/"
        ) { response ->
            response.handler { body ->
                context.assertTrue(body.toString().contains("Hello"))
                async.complete()
            }
        }
    }
}