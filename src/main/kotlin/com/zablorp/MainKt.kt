package com.zablorp

import io.vertx.kotlin.lang.DefaultVertx
import io.vertx.kotlin.lang.bodyJson
import io.vertx.kotlin.lang.httpServer
import io.vertx.kotlin.lang.json.object_

/**
 * Created by mattlangreder on 6/20/17.
 */
class MainKt {
    companion object {
        @JvmStatic fun main(args: Array<String>) {
            DefaultVertx {
                httpServer(8084) { request ->
                    bodyJson {
                        object_(
                                "title" to "Hello, my remote peer",
                                "message" to "You address is ${request.remoteAddress().host()}"
                        )
                    }
                }
            }
        }
    }
}


